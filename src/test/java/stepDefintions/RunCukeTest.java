package stepDefintions;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"src/test/resources/features/ui",
					"src/test/resources/features/eas-web-fe"},
		glue = {"info.seleniumcucumber.stepdefinitions"},
		plugin = {"pretty","html:target/cucumber-reports/cucumberHtmlReport",
                "json:target/cucumber-reports/cucumber-json-report.json",
		        "junit:target/cucumber-reports/Cucumber.xml"},
		monochrome = true,
		strict = true

)

public class RunCukeTest {

}
