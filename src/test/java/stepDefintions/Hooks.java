package stepDefintions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import env.SharedDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;

public class Hooks {

    @After
    public void afterScenario(Scenario scenario, SharedDriver webDriver){

        if(scenario.isFailed()) {
            try {
                scenario.write("Current Page URL is " + webDriver.getCurrentUrl());
//            byte[] screenshot = getScreenshotAs(OutputType.BYTES);
                byte[] screenshot = webDriver.getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
                System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            }

        }
        webDriver.quit();
    }

}
