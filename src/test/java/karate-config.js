function() {
  var env = karate.env; // get java system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'qa'; // a custom 'intelligent' default
  }
  var config = { // base config JSON
    appId: 'Agilquest Forum',
    testURL: 'http://209.201.33.130:8080',
  };
  if (env == 'dev') {
    // over-ride only those that need to be
    config.testURL = 'http://209.201.33.16:8080';
  } else if (env == 'uat') {
    config.testURL = 'http://209.201.33.148:8080';
  }
  // don't waste time waiting for a connection or if servers don't respond within 5 seconds
  karate.configure('connectTimeout', 5000);
  karate.configure('readTimeout', 5000);
  return config;
}