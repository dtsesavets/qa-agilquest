Feature:  plai_login_without_profile

	Scenario: deviceOwnership
	  Given url "http://plai.agilquest.com:8081/plai-listener/"
			And path 'PSIA', 'CSEC', 'deviceOwnership'
			And params { 'OwnerGUID': '39103DAD-DC5E-4442-96F2-B4E6EC4C88A0' }
			And headers { "accept": "application/xml", "content-type": "application/xml", "Authorization": "Basic aXRlY2hhcnQ6UGhvZW5peDIzIQ==" }
	  When method GET
	  Then status 200