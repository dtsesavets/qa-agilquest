Feature: ui_plai_login
  
Scenario: PSIA_PROFILE
	
	Given url "http://plai.agilquest.com:8081/plai-listener/"
		And path 'PSIA', 'Profile'
		And headers { "accept": "application/xml", "content-type": "application/xml", "Authorization": "Basic aXRlY2hhcnQ6UGhvZW5peDIzIQ==" }
	When method GET
	Then status 200
	
	Scenario: deviceOwnership
	  Given url "http://plai.agilquest.com:8081/plai-listener/"
	  And path 'PSIA', 'CSEC', 'deviceOwnership'
	  And params { 'OwnerGUID': '39103DAD-DC5E-4442-96F2-B4E6EC4C88A0' }
	  And headers { "accept": "application/xml", "content-type": "application/xml", "Authorization": "Basic aXRlY2hhcnQ6UGhvZW5peDIzIQ==" }
	  When method GET
	  Then status 200
	  And def authSignature = response.IssuerSignature
	  And def myParam = response.IssuerSignature

  Scenario: mediaStream
	Given url "http://plai.agilquest.com:8081/plai-listener/"
	And path 'PSIA', 'Metadata', 'clientStream'
	And params { 'OwnerGUID': '{39103DAD-DC5E-4442-96F2-B4E6EC4C88A0}' }
	And headers { "accept": "application/xml", "content-type": "application/xml", "Authorization": "Basic aXRlY2hhcnQ6UGhvZW5peDIzIQ==", "IssuerSignature": '#(authSignature)'}
	And request <?xml version="1.0" encoding="utf-8"?><AreaControlEventList><AreaControlEvent><MetadataHeader><MetaVersion>1</MetaVersion><MetaID>/psialliance.org/AreaControl.Portal/access.granted/20775096</MetaID><MetaSourceID>39103DAD-DC5E-4442-96F2-B4E6EC4C88A0</MetaSourceID><MetaSourceLocalID>20775096</MetaSourceLocalID><MetaTime>2018-06-26T14:48:47.1068232-04:00</MetaTime><MetaPriority>4</MetaPriority></MetadataHeader><EventData><ValueState><Granted>OK</Granted></ValueState><PortalID><ID>706</ID><Name>DC0989 Reader 706</Name></PortalID><CredentialInfo><ID>20775096</ID><UID>{AABBCCDD-00000-0000-0000-123456789012}</UID><State>Inactive</State><IdentifierInfoList /></CredentialInfo><CredentialHolderInfo><ID>20784375</ID><UID>{55E0BB4D-6015-4572-84EC-5E79BE969DCC}</UID><Name>AkdemirAkdemir</Name><GivenName>Ugur</GivenName><Surname>Akdemir</Surname><ActiveFrom>0001-01-01T00:00:00</ActiveFrom><ActiveTill>0001-01-01T00:00:00</ActiveTill><State>Inactive</State><RoleIDList /><AttributeList><Attribute><Name>Email</Name><Value>uakdemir@kastle.com</Value></Attribute></AttributeList></CredentialHolderInfo><Info>DC0989 Lobby Level, H6 Reader 706</Info></EventData></AreaControlEvent><AreaControlEvent><MetadataHeader><MetaVersion>1</MetaVersion><MetaID>/psialliance.org/AreaControl.Portal/access.granted/220012</MetaID><MetaSourceID>{BF4A1CCB-BAE1-4C44-B04A-32DF21CCB14E}</MetaSourceID><MetaSourceLocalID>220012</MetaSourceLocalID><MetaTime>2018-06-26T14:49:32.8112435-04:00</MetaTime><MetaPriority>0</MetaPriority></MetadataHeader><EventData><ValueState><Granted>OK</Granted></ValueState><PortalID><ID>220012</ID><Name>DC0989 Reader 520</Name></PortalID><CredentialHolderInfo><ID>301321123</ID><UID>{12345678-1234-1234-1234-123456789012}</UID><Name>Doe, John</Name><GivenName>John</GivenName><Surname>Doe</Surname><ActiveFrom>0001-01-01T00:00:00</ActiveFrom><ActiveTill>0001-01-01T00:00:00</ActiveTill><State>Active</State><AttributeList><Attribute><Name>Email</Name><Value>john.doe@foo.com</Value></Attribute></AttributeList></CredentialHolderInfo><Info>DC0989 Reader 520</Info></EventData></AreaControlEvent></AreaControlEventList>
	When method POST
	Then status 201