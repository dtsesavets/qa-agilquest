@RESTFUL
@QA
Feature: api_post_users_search
  
  
  Background:
	* url testURL
	
	Given path 'auth', 'application', 'login'
	And headers { "accept": "application/json", "content-type": "application/json" }
	And request { "appLogin": "agilquest.com","appPassword": "AgilquestApplication","accountLogin": "","accountPassword": "" }
	When method POST
	Then status 200
	And def appAuthToken = response.authToken
  
  Scenario:  POST_USERS_SEARCH
	
	Given path 'auth', 'login'
		And headers { "accept": "application/json", "content-type": "application/json", "AQOB-AppAuthToken": '#(appAuthToken)' }
		And request { "emailAddress": "admin@itechart.com", "password": "AqTemp23!" }
	When method POST
	Then status 200
		And def userAuthToken = response.userData.authToken
  
	Given path 'aq-api', 'users', 'search'
	And headers { "accept": "application/json", "content-type": "application/json", "AQOB-AppAuthToken": '#(appAuthToken)', "Authorization": "#(userAuthToken)" }
	And request { "sysidUser": "4", "emailAddress": "admin@itechart.com" }
	When method POST
	Then status 200
	And def sysIDUserID = response.profile.userId
	And def usrEmailAddress = response.account.emailAddress
	