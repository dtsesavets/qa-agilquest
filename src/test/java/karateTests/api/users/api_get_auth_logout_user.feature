@RESTFUL
  @QA
Feature: api_get_auth_logout_user
  
  
  Background:
	* url testURL
	
	Given path 'auth', 'application', 'login'
	And headers { "accept": "application/json", "content-type": "application/json" }
	And request { "appLogin": "agilquest.com","appPassword": "AgilquestApplication","accountLogin": "","accountPassword": "" }
	When method POST
	Then status 200
	And def appAuthToken = response.authToken
  
  Scenario:  GET_AUTH_LOGOUT_USER
	
	Given path 'auth', 'login'
	And headers { "accept": "application/json", "content-type": "application/json", "AQOB-AppAuthToken": '#(appAuthToken)' }
	And request { "emailAddress": "admin@itechart.com", "password": "AqTemp23!" }
	When method POST
	Then status 200
	And def userAuthToken = response.userData.authToken
	
	Given path 'auth', 'logout', 'user'
	And headers { "accept": "application/json", "content-type": "application/json", "AQOB-AppAuthToken": '#(appAuthToken)' }
	When method GET
	Then status 204