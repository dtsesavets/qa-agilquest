@RESTFUL
  @QA
Feature: api_post_user_views
	
  Background:
	* url testURL
 
	
	Given path 'auth', 'application', 'login'
		And headers { "accept": "application/json", "content-type": "application/json" }
		And request { "appLogin": "agilquest.com","appPassword": "AgilquestApplication","accountLogin": "","accountPassword": "" }
	When method POST
	Then status 200
		And def appAuthToken = response.authToken
  
  Scenario:  POST_USER_VIEWS
	
	Given path 'auth', 'login'
	  And headers { "accept": "application/json", "content-type": "application/json", "AQOB-AppAuthToken": '#(appAuthToken)' }
	  And request { "emailAddress": "admin@itechart.com", "password": "AqTemp23!" }
	When method POST
	Then status 200
	  And def userAuthToken = response.userData.authToken
  
	Given path 'aq-api', 'users', 'views'
	  And headers { "accept": "application/json", "content-type": "application/json", "AQOB-AppAuthToken": '#(appAuthToken)', "Authorization": '#(userAuthToken)' }
	  And request {"startRow":0,"maxRows":30,"sortColumn":"LAST_NAME","sortAscending":true}
	When method POST
	Then status 200