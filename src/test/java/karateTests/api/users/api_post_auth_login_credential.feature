@RESTFUL
  @QA
Feature: api_post_auth_login_credential
#badgeCredential
  
  Background:
	* url testURL
	Given path 'auth', 'application', 'login'
		And headers { "accept": "application/json", "content-type": "application/json" }
		And request { "appLogin": "agilquest.com","appPassword": "AgilquestApplication","accountLogin": "","accountPassword": "" }
	When method POST
	Then status 200
		And def appAuthToken = response.authToken
  
  Scenario:  POST_AUTH_LOGIN_CREDENTIAL
	
	Given path 'auth', 'login', 'credential'
		And headers { "accept": "application/json", "content-type": "application/json", "AQOB-AppAuthToken": '#(appAuthToken)' }
		And request { "sysidAccount": "4", "credentialData": {"sysidCredentialsType":1, "value":219-29624 } }
	When method POST
	Then status 200
		And def userAuthToken = response.userData.authToken