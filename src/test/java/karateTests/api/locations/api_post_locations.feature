@RESTFUL
  @QA
Feature: api_post_locations
  
 
 
	Background:
	* url testURL
	Given path 'auth', 'application', 'login'
	  And headers { "accept": "application/json", "content-type": "application/json" }
	  And request { "appLogin": "agilquest.com","appPassword": "AgilquestApplication","accountLogin": "","accountPassword": "" }
	When method POST
	Then status 200
		And def appAuthToken = response.authToken
  
  Scenario: API_POST_LOCATIONS
	
	Given path 'auth', 'login'
	  And headers { "accept": "application/json", "content-type": "application/json", "AQOB-AppAuthToken": '#(appAuthToken)' }
	  And request { "emailAddress": "admin@itechart.com", "password": "AqTemp23!" }
	When method POST
	Then status 200
	  And def userAuthToken = response.userData.authToken
	
	
	Given path 'aq-api', 'locations'
	  And headers { "accept": "application/json", "content-type": "application/json", "AQOB-AppAuthToken": '#(appAuthToken)', "Authorization": '#(userAuthToken)' }
	  And request {"locationDataTypeValue":3,"sysidLocatorNode":null,"sysidApplicationUser":35,"sysidAllocRelationshipTypes":[]}
	When method POST
	Then status 200