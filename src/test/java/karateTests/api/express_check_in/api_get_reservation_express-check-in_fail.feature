@RESTFUL
@QA
Feature: api_get_express-check-in_fail
  
  
  Background:
	* url testURL
	Given path 'auth', 'application', 'login'
		And headers { "accept": "application/json", "content-type": "application/json" }
		And request { "appLogin": "plaiService","appPassword": "AqTemp23!","accountLogin": "","accountPassword": "" }
	When method POST
	Then status 200
		And def appAuthToken = response.authToken
  
  Scenario:  GET_EXPRESS-CHECK-IN_FAIL
	Given path 'auth', 'login'
		And headers { "accept": "application/json", "content-type": "application/json", "AQOB-AppAuthToken": '#(appAuthToken)' }
		And request { "emailAddress": "todd.mcghee@agilquest.com", "password": "AqTemp23!" }
	When method POST
	Then status 200
		And def userAuthToken = response.userData.authToken
	
	Given path 'aq-api', 'reservations', 'express-check-in'
		And param SYSID_Venue = 1
		And headers { "accept": "application/", "content-type": "application/json", "AQOB-AppAuthToken": '#(appAuthToken)', "Authorization": '#(userAuthToken)' }
	When method GET
	Then status 400