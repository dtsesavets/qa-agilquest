Feature: ui_eas_login
  
  Background: 
	Given I navigate to "http://209.201.32.246:8080/eas-web-fe/login"
	When I wait 3 seconds for element having xpath "//div[@class='main-content']" to display
	Then I should see page title as "saml management"
	
	Scenario: LOGIN_TO_SAML_MANAGEMENT
	  Given I enter "admin@itechart.com" into input field having xpath "//input[@name='loginId']"
	  	And I enter "AqTemp23!" into input field having xpath "//input[@name='password']"
	  When I click on element having xpath "//button[@type='submit']"
	  Then element having xpath "//a[@class='a-btn button-primary toolbar-btn'][contains(text(),'VIEW SP METADATA')]" should be present
	  	And element having xpath "//a[@class='a-btn button-primary toolbar-btn'][contains(text(),'LOGOUT')]" should be present
	  	And element having xpath "//input[@id='account-name']" should be present
	  	And element having xpath "//div[@class='col-md-3 col-sm-3 col-xs-4']" should be present
	  
	Scenario: DISABLED_LOGIN_BUTTON
	  Given I enter "admin@itechart.com" into input field having xpath "//input[@name='loginId']"
	  	And I wait for 2 sec
	  Then element having xpath "//button[@type='submit']" should not be enabled
	  
	  Scenario: INCORRECT_LOGIN
		Given I enter "a@a.com" into input field having xpath "//input[@name='loginId']"
		When I enter "11223344" into input field having xpath "//input[@name='password']"
			And I click on element having xpath "//button[@type='submit']"
			And I wait for 2 sec
		Then element having xpath "//div[@class='error-message font-error']" should be present
			And element having xpath "//button[@type='submit']" should be disabled
	
	Scenario: LOGOUT
	  Given I enter "admin@itechart.com" into input field having xpath "//input[@name='loginId']"
	  	And I enter "AqTemp23!" into input field having xpath "//input[@name='password']"
	  	And I click on element having xpath "//button[@type='submit']"
		And I wait 3 seconds for element having xpath "//a[@class='a-btn button-primary toolbar-btn'][contains(text(),'LOGOUT')]" to be enabled
	  When I click on element having xpath "//a[@class='a-btn button-primary toolbar-btn'][contains(text(),'LOGOUT')]"
	  Then element having xpath "//div[@class='login-page-content']" should be present
	  