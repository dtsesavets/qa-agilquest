Feature: ui_create_equipment_reservation

Scenario: login_and_create_reservation_for_equipment_asset
    #No need to login again because we are in the same session
Given I navigate to "http://209.201.33.130/signin"
    #Where Widget
When I click on element having xpath "//*[@id="home_whereWidget"]"
 And I wait for 1 sec
    #Venue: facility1_1_1
 And I click on element having xpath "/html[1]/body[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]"
    #Apply button
 And I click on element having xpath "//button[@class='button-primary save-button']"
    #What Widget
 And I click on element having xpath "/html/body/div[1]/div/div/div[1]/div[2]/div[1]/div[3]/div[2]/div/div[1]/div/div[2]/div[2]/div/input"
 And I wait for 2 sec
    #Equipment
 And I click on element having xpath "//div[6]/div/div/div[2]/div/div/div/div/div[3]"
 And I wait for 5 sec
   #Search By List Button
 And I click on element having xpath "//button[@id='searchByListBtn']"
    #AssetByl - facility1_1_1
 And I wait for 2 sec
 And I click on element having xpath "/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div/div[3]/div[1]/div/div[1]/div[1]/div/div[1]/div/div/div[2]/p[2]/a"
 And I wait for 5 sec
 And I switch to new window
 And I wait 5 seconds for element having id "name" to display
   #Reservation Name field
 And I enter "QA Automation Reservation" into input field having id "name"
 And I wait for 2 sec
  And I enter "1" into input field having xpath " //input[@id='res-quantity']"
  And I wait for 1 sec
 And I enter "QA Automation Test Equipment Description" into input field having xpath "	//textarea[@id='res-delivery']"
    #Submit Button
 Then I click on element having xpath "	//button[@class='button-primary']"
 #TODO Check for Reservation Error or Confirmation
 And I wait for 2 sec
 And I close new window
 And I switch to previous window
	