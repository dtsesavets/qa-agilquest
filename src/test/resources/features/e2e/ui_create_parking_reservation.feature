Feature: ui_create_parking_reservation

Scenario: login_and_create_reservation_for_parking_asset
    #No need to login again because we are in the same session
Given I navigate to "http://209.201.33.130/signin"
  And I wait for 5 sec
    #Where Widget
When I click on element having xpath "//*[@id="home_whereWidget"]"
  And I wait for 1 sec
    #Venue: facility1_1_1
  And I click on element having xpath "/html/body/div[4]/div/div/div[2]/div/div[1]/div[1]/div/div[1]/div/div/div/div/div"
    #Apply button
	And I click on element having xpath "//button[@class='button-primary save-button']"
    #What Widget
	And I click on element having xpath "/html/body/div[1]/div/div/div[1]/div[2]/div[1]/div[3]/div[2]/div/div[1]/div/div[2]/div[2]/div/input"
	And I wait for 2 sec
    #Parking
	And I click on element having xpath "//div[@data-idx='4']"
	And I wait for 4 sec
   #Search By List Button
	And I click on element having xpath "//button[@id='searchByListBtn']"
    #AssetByl - facility1_1_1
	And I wait for 2 sec
	And I click on element having xpath "/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div/div[3]/div[1]/div/div[1]/div[1]/div/div[1]/div/div/div[2]/p[2]/a"
	And I wait for 5 sec
  	And I switch to new window
	And I wait for 2 sec
   #Reservation Name field
	And I enter "QA Automation Reservation" into input field having id "name"
	And I wait for 1 sec
	And I enter "QA Automation Test Parking Description" into input field having id "description"
    #Submit Button
Then I click on element having xpath "//button[@class='button-primary']"
	#TODO Check for Reservation Error or Confirmation
	And I close new window
  	And I switch to previous window