Feature: ui_create_collab_space_reservation
  
Scenario: login_and_create_reservation_for_collaboration_space
	Given I switch to new window
		And I navigate to "http://209.201.33.130/signin"
		And I wait for 5 sec
		And I enter "admin@itechart.com" into input field having xpath "/html/body/div[1]/div/div/div[1]/div[1]/div/form/div[1]/input"
		And I enter "AqTemp23!" into input field having xpath "/html/body/div[1]/div/div/div[1]/div[1]/div/form/div[2]/input"
		And I click on element having class "login-btn"
		And I wait for 4 sec
	#Where Widget
		And I click on element having xpath "//*[@id="home_whereWidget"]"
		And I wait for 2 sec
	#Venue: facility1_1_1
		And I click on element having xpath "/html/body/div[4]/div/div/div[2]/div/div[1]/div[1]/div/div[1]/div/div/div/div/div"
	#Apply button
		And I click on element having xpath "/html/body/div[4]/div/div/div[2]/div/div[3]/button[2]"
  	#Collaboration Space
		And I click on element having xpath "//*[@id="home_assetCategory"]"
		And I wait for 3 sec
		And I click on element having class "drop-down-option"
    #Search By List Button
		And I click on element having xpath "//button[@id='searchByListBtn']"
	#Asset_cOll1 - facility1_1_1
		And I wait for 5 sec
		And I click on element having xpath "/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div/div[3]/div[1]/div/div[1]/div[1]/div/div[1]/div/div/div[2]/p[2]/a"
	When I switch to new window
  		And I switch to main content
  		And I wait 5 seconds for element having id "name" to display
   #Reservation Name field
		And I enter "QA Automation Reservation" into input field having id "name"
		And I wait for 2 sec
		And I enter "1" into input field having id "people-num"
		And I enter "QA Automation Test Collaboration Space Description" into input field having id "description"
	#Submit Button
	Then I click on element having xpath "/html/body/div[1]/div/div/div[1]/div[2]/div[1]/div/div[2]/div[1]/div[1]/div/div/div/div[2]/div[3]/div[2]"
  		And I wait for 3 sec
  		And I close new window