Feature: ui_aqcom_forget_password
  As a User, I want to recover my password if I forget it, so I can log in again
  
  Scenario: ui_aqcom_forget_password_test
	Given I navigate to "http://209.201.33.130/signin"
	And I wait for 5 sec
	When I click on element having css ".forgot-pass-link"
	Then element having css ".sign-form-container" should be present