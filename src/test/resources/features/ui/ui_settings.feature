Feature: ui_settings
  
  Background: Logging In
	Given I navigate to "http://209.201.33.130/signin"
		And I wait for 5 sec
		And I enter "admin@itechart.com" into input field having xpath "//input[@name='loginId']"
		And I enter "AqTemp23!" into input field having xpath "//input[@name='password']"
	When I click on element having xpath "//button[@type='submit']"
		And I wait for 4 sec
		And element having css ".user-name" should have text as "Peter Smith"
	Then I click on element having xpath " //a[@class='button inline'][contains(text(),'Settings')]"
		And I wait for 4 sec
	
  Scenario: Click on Settings - Reservations
	Given element having xpath "//div[@class='activity-window-title']" should have text as "Reservations: Table"
		And element having xpath "//input[@id='BaseSearchForm_whereWidget']" should have attribute "placeholder" with value "Where"
		And element having xpath "//input[@value='What']" should have attribute "placeholder" with value "What"
	When I click on element having xpath "//button[@class='button-primary search-button-mobile']"
	   	And I wait for 2 sec
	Then I wait 5 seconds for element having xpath "//div[@class='table-container reservation-grid']" to display
		And I click on element having xpath "//div[@class='user-name']"
		And I wait for 5 sec
		And I click on element having xpath "//a[@href='/'][contains(text(),'Logout')]"
		And I should see page title as "agilquest"
	
	