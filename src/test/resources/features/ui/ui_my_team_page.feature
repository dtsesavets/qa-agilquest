Feature: MY_TEAM_PAGE
  
  Background:
	Given I navigate to "http://209.201.33.130/signin"
		And I wait for 5 sec
	When I enter "admin@itechart.com" into input field having xpath "//input[@name='loginId']"
		And I enter "AqTemp23!" into input field having xpath "//input[@name='password']"
  		And I click on element having xpath "//button[@type='submit']"
		And I wait for 4 sec
		And I click on element having xpath "//div[@class='user-name']"
  	Then I click on element having xpath "//a[@href='/profile']"
	
	Scenario: Go_To_My_Team
	  Given I click on element having xpath "//a[@class='my-profile-team-tab']"
	  Then element having xpath "//div[@class='activity-window-title']" should be present
	  	And element having xpath "//button[@id='contacts-add-user']" should be present
	  	And element having xpath "//div[@class='flex-item']" should be present