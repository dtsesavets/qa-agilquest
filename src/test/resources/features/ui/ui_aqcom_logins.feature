Feature: ui_aqcom_login

  Scenario: I login with valid credential
    Given I navigate to "http://209.201.33.130/signin"
      And I wait for 5 sec
      And I enter "admin@itechart.com" into input field having xpath "//input[@name='loginId']"
      And I enter "AqTemp23!" into input field having xpath "//input[@name='password']"
    When I click on element having xpath "//button[@type='submit']"
      And I wait for 4 sec
    Then element having css ".user-name" should have text as "Peter Smith"
    

  Scenario: I logout of site
    Given I click on element having xpath "//div[@class='user-name']"
    When I wait 4 seconds for element having xpath "//a[@href='/'][contains(text(),'Logout')]" to display
    Then I click on element having xpath "//a[@href='/'][contains(text(),'Logout')]"
      And I wait for 4 sec
    
  Scenario: I login with a old password
    Given I navigate to "http://209.201.33.130/signin"
      And I wait for 5 sec
      And I enter "admin@itechart.com" into input field having xpath "//input[@name='loginId']"
      And I enter "AqTemp23!!" into input field having xpath "//input[@name='password']"
    When I click on element having class "login-btn"
      And I wait for 4 sec
      And element having css ".popup" should be present
  
  Scenario: I click login without username or password
    Given I navigate to "http://209.201.33.130/signin"
    And I click on element having class "login-btn"
    When element having css ".error-content" should have text as "Invalid Email Address and/or Password"
    Then I click on element having css ".control-bt-1"
            
            
