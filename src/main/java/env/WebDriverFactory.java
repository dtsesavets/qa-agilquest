package env;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import java.util.Arrays;

import static java.util.stream.Collectors.toList;

/**
 * Created by tom on 24/02/17.
 */
final class WebDriverFactory {

	public WebDriverFactory() {

	}

	static WebDriver create() {
		String chromeProperty = System.setProperty("webdriver.chrome.driver", "C:\\Driver\\chromedriver.exe");
		String firefoxProperty = System.setProperty("webdriver.gecko.driver", "C:\\Driver\\geckodriver.exe");


		if (System.getProperty("browser", "firefox") == null || System.getProperty("browser", "chrome") == null) {
			throw new IllegalStateException(("The webdriver system property must be set"));
		}

		try {
			return Drivers.valueOf(System.getProperty("browser", "firefox").toUpperCase()).newDriver();
		} catch (IllegalArgumentException e) {
			String msg = String.format("The webdriver system property '%s' did not match any " +
							"existing browser or the browser was not supported on your operating system. " +
							"Valid values are %s",
					firefoxProperty, Arrays.stream(Drivers
							.values())
							.map(Enum::name)
							.map(String::toLowerCase)
							.collect(toList()));

			throw new IllegalStateException(msg, e);
		}
	}

	private enum Drivers {
		FIREFOX {
			@Override
			public WebDriver newDriver() {
				FirefoxOptions options = new FirefoxOptions();
				try {
					FirefoxProfile fp = new FirefoxProfile();
					options.setCapability(FirefoxDriver.PROFILE, fp);
					options.setCapability("marionette", true);
					options.setCapability("pref.privacy.disable_button.view_passwords_exceptions", false);
				} catch (Exception e) {
					e.printStackTrace();
				}

				return new FirefoxDriver(options);
			}
		}, CHROME {
			@Override
			public WebDriver newDriver() {
				ChromeOptions options = new ChromeOptions();
				return new ChromeDriver(options);
			}
		}, IE {
			@Override
			public WebDriver newDriver() {
				InternetExplorerOptions options = new InternetExplorerOptions();
				return new InternetExplorerDriver(options);
			}
		};

		public abstract org.openqa.selenium.WebDriver newDriver();

	}

}





