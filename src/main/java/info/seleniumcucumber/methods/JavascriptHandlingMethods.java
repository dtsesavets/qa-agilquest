package info.seleniumcucumber.methods;

import env.SharedDriver;

public class JavascriptHandlingMethods implements BaseTest {

	/**Method to handle alert
	 * @param decision : String : Accept or dismiss alert
	 */
	public void handleAlert(String decision, SharedDriver webDriver)
	{
		if(decision.equals("accept"))
			webDriver.switchTo().alert().accept();
		else
			webDriver.switchTo().alert().dismiss();
	}
}
