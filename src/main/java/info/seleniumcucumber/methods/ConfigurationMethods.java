package info.seleniumcucumber.methods;

import env.SharedDriver;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ConfigurationMethods implements BaseTest
{	  

	public ConfigurationMethods() {

	}
	/** Method to print desktop configuration	 */
	public void printDesktopConfiguration(SharedDriver webDriver)
	{
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		Calendar cal = Calendar.getInstance();
		
		System.out.println("Following are machine configurations : \n");
		System.out.println("Date (MM/DD/YYYY) and Time (HH:MM:SS) : "+dateFormat.format(cal.getTime()));
		System.out.println("WebDriver is " + webDriver.toString() + ".");
	}
}
