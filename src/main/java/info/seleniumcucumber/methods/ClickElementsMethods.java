package info.seleniumcucumber.methods;

import env.SharedDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ClickElementsMethods extends SelectElementByType implements BaseTest
{
	//SelectElementByType eleType= new SelectElementByType();
	private WebElement element=null;

	
	/** Method to click on an element
	@param accessType : String : Locator type (id, name, class, xpath, css)
	@param accessName : String : Locator value
	*/
	public void click(String accessType, String accessName, WebDriverWait wait)
	{

		element = wait.until(ExpectedConditions.visibilityOfElementLocated(getelementbytype(accessType, accessName)));
		element.click();
	}
	
	/** Method to forcefully click on an element
	@param accessType : String : Locator type (id, name, class, xpath, css)
	@param accessName : String : Locator value
	*/
	public void clickForcefully(String accessType, String accessName, SharedDriver webDriver, WebDriverWait wait)
	{
		element = wait.until(ExpectedConditions.presenceOfElementLocated(getelementbytype(accessType, accessName)));
		JavascriptExecutor executor = webDriver;
		executor.executeScript("arguments[0].click();",element);
	}
	
	/** Method to Double click on an element
	@param accessType : String : Locator type (id, name, class, xpath, css)
	@param accessValue : String : Locator value
	*/
	public void doubleClick(String accessType, String accessValue, SharedDriver webDriver, WebDriverWait wait)
	{
		element = wait.until(ExpectedConditions.presenceOfElementLocated(getelementbytype(accessType, accessValue)));

		Actions action = new Actions(webDriver);
		action.moveToElement(element).doubleClick().perform();
	}
}