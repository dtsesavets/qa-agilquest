﻿[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$geckoUrl = "https://github.com/mozilla/geckodriver/releases/download/v0.21.0/geckodriver-v0.21.0-win64.zip"
$chromeUrl = "https://chromedriver.storage.googleapis.com/2.40/chromedriver_win32.zip"

$geckoOutput = "C:\Driver\geckodriver-v0.21.0-win64.zip"
$geckoDestination = "C:\Driver\"

$chromeOutput = "C:\Driver\chromedriver-win32.zip"
$chromeDestination = "C:\Driver\"

$driverDir = "C:\Driver"
if(-Not (Test-Path -Path $driverDir)){
    Set-ExecutionPolicy Unrestricted -Scope CurrentUser
    New-Item -ItemType Directory -Path $driverDir
}
echo "Directory now exists at $geckoDestination"

Set-ExecutionPolicy Unrestricted -Scope CurrentUser
echo "Downloading Geckodriver to $geckoDestination from $geckoUrl"
Invoke-WebRequest -UseBasicParsing -Uri $geckoUrl -OutFile $geckoOutput

echo "Unzipping Geckodriver to $geckoDestination"
Set-ExecutionPolicy Unrestricted -Scope CurrentUser
Expand-Archive $geckoOutput -DestinationPath $geckoDestination -Force

echo "Removing Zip file from $geckoDestination because I'm nice"
Set-ExecutionPolicy Unrestricted -Scope CurrentUser
Remove-Item $geckoOutput -Force

Set-ExecutionPolicy Unrestricted -Scope CurrentUser
echo "Downloading Chromedriver to $chromeDestination from $chromeUrl"
Invoke-WebRequest -UseBasicParsing -Uri $chromeUrl -OutFile $chromeOutput

echo "Unzipping Geckodriver to $chromeDestination"
Set-ExecutionPolicy Unrestricted -Scope CurrentUser
Expand-Archive $chromeOutput -DestinationPath $chromeDestination -Force

echo "Removing Zip file from $chromeDestination because I'm nice"
Set-ExecutionPolicy Unrestricted -Scope CurrentUser
Remove-Item $chromeOutput -Force
